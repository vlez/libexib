# libEXIB API V1

The libEXIB API provides an object-oriented interface to EXIB data. This
interface can be divided into three parts: encoder, decoder, and common.

Functions and types are prefixed with their respective area. For example: 
`EXIB_ENC_Context`, `EXIB_DEC_GetLastError`, `EXIB_SetAllocator` are from
encoder, decoder, and common, respectively.

