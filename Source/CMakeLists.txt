target_sources(EXIB PRIVATE Util.c AllocatorInternal.h Allocator.c
        EncoderInternal.h Encoder.c EncoderString.c EncoderObject.c EncoderArray.c
        DecoderInternal.h Decoder.c DecoderString.c DecoderObject.c)
target_sources(EXIB PUBLIC ../Include/EXIB/EXIB.h
        ../Include/EXIB/Encoder.h
        ../Include/EXIB/EncoderTypes.h
        ../Include/EXIB/EncoderArray.h
        ../Include/EXIB/Decoder.h)