#include <stdlib.h>
#include <string.h>
#include <EXIB/EXIB.h>
#include <EXIB/Encoder.h>
#include "AllocatorInternal.h"
#include "EXIB/EncoderTypes.h"
#include "EncoderInternal.h"

int EXIB_ENC_InitializeArray(EXIB_ENC_Context* ctx,
                             EXIB_ENC_Array* array,
                             EXIB_ENC_Object* parent,
                             const char* name,
                             EXIB_Type type)
{
    EXIB_ENC_Field* field = &array->object.field;
    EXIB_ENC_InitializeField(ctx, field, parent, name,
                           EXIB_TYPE_ARRAY);
    
    field->elementType = type;

    // Reserve space if this is an array of values.
    if (EXIB_ENC_ArrayReserve(array, 32))
        return 1;

    return 0;
}

int EXIB_ENC_ArrayReserve(EXIB_ENC_Array* array, uint32_t newCapacity)
{
    EXIB_Type type = array->object.field.elementType;
    int elementSize = EXIB_GetTypeSize(type);

    // Objects and arrays use a linked list instead.
    if (type == EXIB_TYPE_OBJECT || type == EXIB_TYPE_ARRAY)
        return 0;

    // Don't try to shrink the array.
    if (newCapacity < array->elementCapacity)
        return 0;

    if (!array->valueElements)
    {
        // Allocate array from scratch.
        array->elementCapacity = newCapacity;
        array->valueElements = EXIB_Calloc(elementSize, 
                                           array->elementCapacity);
        if (!array->valueElements)
            return 1;
        return 0;
    }

    void* elements = EXIB_Calloc(elementSize, newCapacity);
    if (!elements)
        return 1;

    // Copy data into new buffer, then free old one.
    memcpy(elements, array->valueElements, elementSize * array->elementCapacity);
    EXIB_Free(array->valueElements);
    array->valueElements = elements;
    array->elementCapacity = newCapacity;

    return 0;
}

size_t EXIB_ENC_ArrayGetSize(EXIB_ENC_Array* array)
{
    return array->elementCount;
}

void* EXIB_ENC_ArrayGetData(EXIB_ENC_Array* array)
{
    return array->valueElements;
}

void* EXIB_ENC_ArraySet(EXIB_ENC_Array* array, size_t index, EXIB_Value value)
{
    int elementSize = EXIB_GetTypeSize(array->object.field.elementType);
    
    if (index >= array->elementCount)
        return NULL;

    if (elementSize == 1)
        ((uint8_t*)array->valueElements)[index] = value.uint8;
    else if (elementSize == 2)
        ((uint16_t*)array->valueElements)[index] = value.uint16;
    else if (elementSize == 4)
        ((uint32_t*)array->valueElements)[index] = value.uint32;
    else if (elementSize == 8)
        ((uint64_t*)array->valueElements)[index] = value.uint64;

    return NULL;
}

void* EXIB_ENC_ArrayAppend(EXIB_ENC_Array* array, EXIB_Value value)
{
    if (array->elementCount == array->elementCapacity)
    {
        if (!EXIB_ENC_ArrayReserve(array, array->elementCapacity + 32))
            return NULL;
    }

    return EXIB_ENC_ArraySet(array, array->elementCount++, value);
}

EXIB_ENC_Object* EXIB_ENC_ArrayAddObject(EXIB_ENC_Context* ctx, EXIB_ENC_Array* array, const char* name)
{
    return EXIB_ENC_AddObject(ctx, &array->object, name);
}