#ifndef _TEST_DEC_H
#define _TEST_DEC_H

#include <stdint.h>

// Smallest possible EXIB datum, an empty root object.
static const uint8_t DEC_EmptyRoot[] = { 
    0xe4, 0x1b, 0x01, 0x00, 0x14, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x0f, 0x00, 0x00, 0x00 };

// Smallest possible EXIB datum, an empty root object.
static const uint8_t DEC_InvalidRoot[] = { 
    0xe4, 0x1b, 0x01, 0x00, 0x14, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// Simple datum with two int32 fields.
static const uint8_t DEC_Numbers[] = {
  0xE4, 0x1B, 0x01, 0x00, 0x31, 0x00, 0x00, 0x00, 
  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x0F, 0x00, 0x14, 0x00, // Root Object, unnamed, 20 bytes
  0x36, 0x00, 0x00, 0x00, // a: uint32, 1 padding byte
  0xEF, 0xBE, 0xAD, 0xDE, // 0xDEADBEEF
  0x36, 0x03, 0x00, 0x00, // b: uint32, 1 padding byte
  0xBE, 0xBA, 0xFE, 0xCA, // 0xCAFEBABE
  0x11, 0x06, 0x00,       // c: int8
  0x64,                   // 'd'
  0x01, 0x00, 0x61, 0x01, 0x00, 0x62, 0x01, 0x00, 0x63 // String Table
};

// Datum with various numbers and nested objects.
static const uint8_t DEC_NumbersAndObjects[128] = {
	0xE4, 0x1B, 0x01, 0x00, 0x80, 0x00, 0x00, 0x00, 0x2C, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x40, 0x00, 0x36, 0x00, 0x00, 0x00,
	0xEF, 0xBE, 0xAD, 0xDE, 0x36, 0x03, 0x00, 0x00, 0xBE, 0xBA, 0xFE, 0xCA,
	0x11, 0x06, 0x00, 0x64, 0x1F, 0x09, 0x00, 0x00, 0x06, 0x00, 0x1F, 0x1B,
	0x00, 0x00, 0x00, 0x00, 0x1F, 0x12, 0x00, 0x00, 0x1A, 0x00, 0x79, 0x23,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x3F, 0x39, 0x26, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x3F, 0x39, 0x29, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x01, 0x00, 0x61, 0x01, 0x00, 0x62, 0x01, 0x00, 0x63, 0x07, 0x00, 0x6F,
	0x62, 0x6A, 0x65, 0x63, 0x74, 0x31, 0x07, 0x00, 0x6F, 0x62, 0x6A, 0x65,
	0x63, 0x74, 0x32, 0x06, 0x00, 0x6E, 0x65, 0x73, 0x74, 0x65, 0x64, 0x01,
	0x00, 0x78, 0x01, 0x00, 0x79, 0x01, 0x00, 0x7A
};

#endif // _TEST_DEC_H
